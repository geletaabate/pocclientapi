﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocClientAPI
{
    public class GxAssaye 
    {
        public int Id { get; set; }
        public int test_ID { get; set; }
        public int assay_ID { get; set; }
        public bool scanned_SID { get; set; }
        public int scanned_PID { get; set; }
        public string notes { get; set; }
        public int test_type {get;set;}

        public int expected_result { get; set; }
        public string guid { get; set; }
        public string order_time { get; set; }
        public bool outliner { get; set; }
        public bool archived { get; set; }
        public bool auto_archived { get; set; }
        public string creation { get; set; }
        public string modified { get; set; }
        public string sample_type_key { get; set; }
        public string other_sample_type_text { get; set; }
        public string result_text { get; set; }
        public string result_text_colors { get; set; }
        public string express_result_text { get; set; }
        public int order_id { get; set; }
        public int uploaded_status { get; set; }
        public int priority { get; set; }
        public int cartridge_order_id { get; set; }
        public int data_reduction_alg { get; set; }
        public int state { get; set; }
        public int error_status { get; set; }
        public string site_name { get; set; }
        public string site_serial_num { get; set; }
        public string operator_ids { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public string assay_name { get; set; }
        public int assay_version { get; set; }
        public int assay_type { get; set; }
        public string test_code { get; set; }
        public string status { get; set; }
    }
}
