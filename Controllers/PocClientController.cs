﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PocClientAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PocClientController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private IConfiguration _configuration;

        public PocClientController(ILogger<WeatherForecastController> logger,IConfiguration configuration )
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public Response PostGxAssay(GxAssay gxAssay)
        {
            Response myresponse = new Response();

            try
            {
                var res=Save(gxAssay);

                if (res != null)
                {
                    myresponse.Id = gxAssay.test_ID;
                    myresponse.IsSuccess = true;
                }
                else
                {

                    myresponse.Id = 0;
                    myresponse.IsSuccess = false;
                }

                return myresponse;
            }
            catch(Exception ex)
            {
                myresponse.Id = 0;
                myresponse.IsSuccess = false;
                return myresponse;
            }
        }


        public object Save(GxAssay gxAssay)
        {
            
            string sql = @" 
                                    INSERT INTO [dbo].[GxAssay]
                                           ([test_ID]  ,[assay_ID] ,[sample_ID] ,[scanned_SID] ,[scanned_PID]
                                           ,[notes]  ,[test_type]  ,[expected_result] ,[lis_upload_message_ID] ,[guid]
                                           ,[order_time],[outliner] ,[archived]  ,[auto_archived]
                                           ,[creation]  ,[modified] ,[sample_type_key]  ,[other_sample_type_text]
                                           ,[result_text] ,[result_text_colors] ,[express_result_text] ,[order_id]
                                           ,[upload_status] ,[priority] ,[cartridge_order_id] ,[data_reduction_alg]
                                           ,[state]  ,[error_status]  ,[site_name]  ,[site_serial_num]
                                           ,[operator_ids] ,[start_time] ,[end_time]  ,[assay_name]
                                           ,[assay_version] ,[assay_type] ,[test_code]  ,[status]
                                            , [sitename] , [deviceserial])
                                     VALUES
                                           (
		                                   @test_ID,@assay_ID,@sample_ID ,@scanned_SID ,@scanned_PID
                                           ,@notes,@test_type  ,@expected_result   ,@lis_upload_message_ID  ,@guid
                                           ,@order_time  ,@outliner   ,@archived ,@auto_archived
                                           ,@creation,@modified  ,@sample_type_key ,@other_sample_type_text
                                           ,@result_text ,@result_text_colors ,@express_result_text ,@order_id
                                           ,@upload_status ,@priority  ,@cartridge_order_id,@data_reduction_alg
                                           ,@state   ,@error_status  ,@site_name ,@site_serial_num
                                           ,@operator_ids,@start_time  ,@end_time ,@assay_name
                                           ,@assay_version ,@assay_type ,@test_code     ,@status
                                           ,@sitename , @deviceserial
		                                   ) 
                                             ";

            //MyService serv = new MyService();
            
                 string constring = _configuration["ConnectionStrings:PocLABConnectionString"];

            SqlConnection con = new SqlConnection(constring);
            
                con.Open();

            SqlCommand cm = new SqlCommand(sql, con);
          
               
                SetInsetGxAssay(cm, gxAssay);

                var id =(int)cm.ExecuteNonQuery();
               
           
            con.Close();
            return id;
           
        }


        private static void SetInsetGxAssay(SqlCommand cm, GxAssay gxAssay)
        {
            try
            {
                DatabaseHelper.InsertInt32Param("@test_ID", cm, gxAssay.test_ID);
                DatabaseHelper.InsertInt32Param("@assay_ID", cm, gxAssay.assay_ID);
                DatabaseHelper.InsertStringNVarCharParam("@sample_ID", cm, gxAssay.sample_ID);
                DatabaseHelper.InsertBooleanParam("@scanned_SID", cm, gxAssay.scanned_SID);
                DatabaseHelper.InsertBooleanParam("@scanned_PID", cm, gxAssay.scanned_PID);
                DatabaseHelper.InsertStringNVarCharParam("@notes", cm, gxAssay.notes);

                DatabaseHelper.InsertInt32Param("@test_type", cm, gxAssay.test_type);
                DatabaseHelper.InsertInt32Param("@expected_result", cm, gxAssay.expected_result);
                //DatabaseHelper.InsertInt32Param("@ReportingYear", cm, gxAssay.ReportingYear);
                DatabaseHelper.InsertStringNVarCharParam("@lis_upload_message_ID", cm, gxAssay.lis_upload_message_ID);
                DatabaseHelper.InsertStringNVarCharParam("@guid", cm, gxAssay.guid);
                DatabaseHelper.InsertStringNVarCharParam("@order_time", cm, gxAssay.order_time);
                DatabaseHelper.InsertBooleanParam("@outliner", cm, gxAssay.outliner);
                DatabaseHelper.InsertBooleanParam("@archived", cm, gxAssay.archived);
                DatabaseHelper.InsertBooleanParam("@auto_archived", cm, gxAssay.auto_archived);

                DatabaseHelper.InsertStringNVarCharParam("@creation", cm, gxAssay.creation);
                DatabaseHelper.InsertStringNVarCharParam("@modified", cm, gxAssay.modified);
                DatabaseHelper.InsertStringNVarCharParam("@sample_type_key", cm, gxAssay.sample_type_key);

                DatabaseHelper.InsertStringNVarCharParam("@other_sample_type_text", cm, gxAssay.other_sample_type_text);
                DatabaseHelper.InsertStringNVarCharParam("@result_text", cm, gxAssay.result_text);
                DatabaseHelper.InsertStringNVarCharParam("@result_text_colors", cm, gxAssay.result_text_colors);
                DatabaseHelper.InsertStringNVarCharParam("@express_result_text", cm, gxAssay.express_result_text);


                DatabaseHelper.InsertInt32Param("@order_id", cm, gxAssay.order_id);
                DatabaseHelper.InsertInt32Param("@upload_status", cm, gxAssay.upload_status);
                DatabaseHelper.InsertInt32Param("@priority", cm, gxAssay.priority);
                DatabaseHelper.InsertInt32Param("@cartridge_order_id", cm, gxAssay.cartridge_order_id);
                DatabaseHelper.InsertInt32Param("@data_reduction_alg", cm, gxAssay.data_reduction_alg);
                DatabaseHelper.InsertInt32Param("@state", cm, gxAssay.state);
                DatabaseHelper.InsertInt32Param("@error_status", cm, gxAssay.error_status);

                DatabaseHelper.InsertStringNVarCharParam("@site_name", cm, gxAssay.site_name);
                DatabaseHelper.InsertStringNVarCharParam("@site_serial_num", cm, gxAssay.site_serial_num);

                DatabaseHelper.InsertStringNVarCharParam("@operator_ids", cm, gxAssay.operator_ids);
                DatabaseHelper.InsertStringNVarCharParam("@start_time", cm, gxAssay.start_time);
                DatabaseHelper.InsertStringNVarCharParam("@end_time", cm, gxAssay.end_time);
                DatabaseHelper.InsertStringNVarCharParam("@assay_name", cm, gxAssay.assay_name);

                DatabaseHelper.InsertInt32Param("@assay_version", cm, gxAssay.assay_version);
                DatabaseHelper.InsertInt32Param("@assay_type", cm, gxAssay.assay_type);

                DatabaseHelper.InsertStringNVarCharParam("@test_code", cm, gxAssay.test_code);
                DatabaseHelper.InsertBooleanParam("@status", cm, gxAssay.status);

                DatabaseHelper.InsertStringNVarCharParam("@sitename", cm, gxAssay.sitename);
                DatabaseHelper.InsertStringNVarCharParam("@deviceserial", cm, gxAssay.deviceserial);
            }
            catch(Exception ex)
            {

            }
        }



    }
}
