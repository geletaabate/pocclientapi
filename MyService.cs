﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PocClientAPI
{
    public class MyService
    {
        private readonly IConfiguration _configuration;
        private static string _config;
        public MyService()
        {
            _config = _configuration["ConnectionStrings:PocLABConnectionString"];
        }

        public virtual string Config
        {
            get { return _config; }
            set { _config = value; }
        }

    }
}
