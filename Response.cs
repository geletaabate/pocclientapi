﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocClientAPI
{
    public class Response
    {
        private bool _IsSuccess = false;
        private string _status;
        private string _Message;
        private object _ResponseData;
        private int _Id;
        private data _Data;

        public Response(bool isSuccess, string message, object data, int id,string status)
        {
            _IsSuccess = isSuccess;
            _Message = message;
            _ResponseData = data;
            _Id = id;
            _status = status;
        }
        public Response()
        {
            Id = 0;
        }

        public virtual data Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public virtual bool IsSuccess{
            get { return _IsSuccess; }
            set { _IsSuccess = value; }
        }

        public virtual string Message {
            get { return _Message; }
            set { _Message = value; }
        }

        public virtual object ResponseData {
            get { return _ResponseData; }
            set { _ResponseData = value; }
        }

        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public virtual string Status
        {
            get { return _status; }
            set { _status = value; }
        }


    }

    public class data
    {
        //{"name":"test","salary":"123","age":"23"}
        private string name;
        private int salary;
        private int age;
        private int id;

        public data() { }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }
        public virtual int Salary
        {
            get { return salary; }
            set { salary = value; }
        }
        public virtual int Age
        {
            get { return age; }
            set { age = value; }
        }

        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
