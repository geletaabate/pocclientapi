﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocClientAPI
{
    public class GxAssay
    {
        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _test_ID;
        public int test_ID
        {
            get { return _test_ID; }
            set { _test_ID = value; }
        }

        private int _assay_ID;
        public int assay_ID
        {
            get { return _assay_ID; }
            set { _assay_ID = value; }
        }

        private string _sample_ID;
        public string sample_ID
        {
            get { return _sample_ID; }
            set { _sample_ID = value; }
        }

        private bool _scanned_SID;
        public bool scanned_SID
        {
            get { return _scanned_SID; }
            set { _scanned_SID = value; }
        }

        private bool _scanned_PID;
        public bool scanned_PID
        {
            get { return _scanned_PID; }
            set { _scanned_PID = value; }
        }

        private string _notes;
        public string notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private int _test_type;
        public int test_type
        {
            get { return _test_type; }
            set { _test_type = value; }
        }

        private int _expected_result;
        public int expected_result
        {
            get { return _expected_result; }
            set { _expected_result = value; }
        }

        private string _lis_upload_message_ID;
        public string lis_upload_message_ID
        {
            get { return _lis_upload_message_ID; }
            set { _lis_upload_message_ID = value; }
        }

        private string _guid;
        public string guid
        {
            get { return _guid; }
            set { _guid = value; }
        }

        private string _order_time;
        public string order_time
        {
            get { return _order_time; }
            set { _order_time = value; }
        }

        private bool _outliner;
        public bool outliner
        {
            get { return _outliner; }
            set { _outliner = value; }
        }

        private bool _archived;
        public bool archived
        {
            get { return _archived; }
            set { _archived = value; }
        }

        private bool _auto_archived;
        public bool auto_archived
        {
            get { return _auto_archived; }
            set { _auto_archived = value; }
        }

        private string _creation;
        public string creation
        {
            get { return _creation; }
            set { _creation = value; }
        }

        private string _modified;
        public string modified
        {
            get { return _modified; }
            set { _modified = value; }
        }

        private string _sample_type_key;
        public string sample_type_key
        {
            get { return _sample_type_key; }
            set { _sample_type_key = value; }
        }

        private string _other_sample_type_text;
        public string other_sample_type_text
        {
            get { return _other_sample_type_text; }
            set { _other_sample_type_text = value; }
        }

        private string _result_text;
        public string result_text
        {
            get { return _result_text; }
            set { _result_text = value; }
        }

        private string _result_text_colors;
        public string result_text_colors
        {
            get { return _result_text_colors; }
            set { _result_text_colors = value; }
        }

        private string _express_result_text;
        public string express_result_text
        {
            get { return _express_result_text; }
            set { _express_result_text = value; }
        }

        private int _order_id;
        public int order_id
        {
            get { return _order_id; }
            set { _order_id = value; }
        }

        private int _upload_status;
        public int upload_status
        {
            get { return _upload_status; }
            set { _upload_status = value; }
        }

        private int _priority;
        public int priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private int _cartridge_order_id;
        public int cartridge_order_id
        {
            get { return _cartridge_order_id; }
            set { _cartridge_order_id = value; }
        }

        private int _data_reduction_alg;
        public int data_reduction_alg
        {
            get { return _data_reduction_alg; }
            set { _data_reduction_alg = value; }
        }

        private int _state;
        public int state
        {
            get { return _state; }
            set { _state = value; }
        }

        private int _error_status;
        public int error_status
        {
            get { return _error_status; }
            set { _error_status = value; }
        }

        private string _site_name;
        public string site_name
        {
            get { return _site_name; }
            set { _site_name = value; }
        }

        private string _site_serial_num;
        public string site_serial_num
        {
            get { return _site_serial_num; }
            set { _site_serial_num = value; }
        }

        private string _operator_ids;
        public string operator_ids
        {
            get { return _operator_ids; }
            set { _operator_ids = value; }
        }

        private string _start_time;
        public string start_time
        {
            get { return _start_time; }
            set { _start_time = value; }
        }

        private string _end_time;
        public string end_time
        {
            get { return _end_time; }
            set { _end_time = value; }
        }

        private string _assay_name;
        public string assay_name
        {
            get { return _assay_name; }
            set { _assay_name = value; }
        }

        private int _assay_version;
        public int assay_version
        {
            get { return _assay_version; }
            set { _assay_version = value; }
        }

        private int _assay_type;
        public int assay_type
        {
            get { return _assay_type; }
            set { _assay_type = value; }
        }

        private string _test_code;
        public string test_code
        {
            get { return _test_code; }
            set { _test_code = value; }
        }

        private bool _status;
        public bool status
        {
            get { return _status; }
            set { _status = value; }
        }


        private string _sitename;
        public string sitename
        {
            get { return _sitename; }
            set { _sitename = value; }
        }

        private string _deviceserial;
        public string deviceserial
        {
            get { return _deviceserial; }
            set { _deviceserial = value; }
        }

        public GxAssay()
        {
            this._id = -1;
        }

    }
}
